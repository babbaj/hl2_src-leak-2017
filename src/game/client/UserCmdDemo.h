//
// Created by babbaj on 4/16/21.
//

#ifndef USERCMDDEMO_H
#define USERCMDDEMO_H

#include "proto_version.h"
#include "tier1/bitbuf.h"
#include "usercmd.h"
#include "demofile/demoformat.h"

// don't care how valve does IO lol
#include <cstdio>
#include <cstdlib>

#include <vector>
#include <utility>


struct UserCmdDemo {
    demoheader_t header;
    std::vector<CUserCmd> cmds;
    int currentCmdIdx;

    UserCmdDemo(const demoheader_t& headerIn, std::vector<CUserCmd>&& vec):
        header(headerIn), cmds(std::move(vec)), currentCmdIdx(0) {}

    static UserCmdDemo* parseFromFile(const char* filename);

    CUserCmd* getNextCmd();
};

CUserCmd* UserCmdDemo::getNextCmd() {
    if (this->currentCmdIdx >= this->cmds.size()) {
        return nullptr;
    } else {
        CUserCmd* out = &this->cmds[this->currentCmdIdx];
        this->currentCmdIdx++;
        return out;
    }
}

// lol
std::pair<std::vector<char>, bool> readWholeFile(const char* fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) {
        return std::pair<std::vector<char>, bool>(std::vector<char>(), false);
    }
    fseek(f, 0, SEEK_END);
    int64_t fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    std::vector<char> buffer(fsize);
    fread(&buffer[0], 1, fsize, f);
    fclose(f);
    return std::pair<std::vector<char>, bool>(buffer, true);
}

inline bool IsControlCommand( unsigned char cmd ) {
    return ( (cmd == dem_signon) || (cmd == dem_stop) ||
             (cmd == dem_synctick) || (cmd == dem_datatables ) ||
             (cmd == dem_stringtables) );
}

const char* skipRawData(const char* data) {
    int len;
    memcpy(&len, data, 4);
    return data + (4 + len);
}

const char* skipPacket(const char* data) {
    return skipRawData(data + (sizeof(democmdinfo_t) + (2 * 4) /*sequence info*/));
}

std::vector<CUserCmd> parseCmds(const char* data, int64_t size) {
    const char* ptr = data;
    std::vector<CUserCmd> out;
    do {
        unsigned char cmd;
        memcpy(&cmd, ptr, 1);
        ptr += 1;
        int tick;
        memcpy(&tick, ptr, 4);
        ptr += 4;

        if ( cmd <= 0 || cmd > dem_lastcmd ) {
            ConMsg("Unexpected command token [%d] in .demo file\n", cmd );
            break;
        }
        //ConMsg("cmd = %d\n", cmd);

        switch (cmd) {
            case dem_synctick:
                break;
            case dem_stop:
                goto exit;
            case dem_consolecmd:
            case dem_datatables:
            case dem_stringtables:
                ptr = skipRawData(ptr);
                break;
            case dem_signon:
            case dem_packet:
                ptr = skipPacket(ptr);
                break;
            case dem_usercmd: {
                //ConMsg("usrcmd!! %d\n", cmd);
                int outgoing_sequence; // not sure what this is for
                memcpy(&outgoing_sequence, ptr, 4);
                ptr += 4;
                int len;
                memcpy(&len, ptr, 4);
                ptr += 4;
                char cmdbuf[256];
                const auto cmdSize = len > sizeof(cmdbuf) ? sizeof(cmdbuf) : len;
                memcpy(cmdbuf, ptr, cmdSize);
                ptr += len;

                bf_read msg("CDemo::ReadUserCmd", cmdbuf, cmdSize);
                CUserCmd cmd;
                CUserCmd nullcmd;
                ReadUsercmd(&msg, &cmd, &nullcmd);
                out.push_back(cmd);
                break;
            }

            default: ;
                //goto exit;
        }

    } while (reinterpret_cast<uintptr_t>(ptr) < (reinterpret_cast<uintptr_t>(data) + size));
    exit:
    return out;
}

inline UserCmdDemo* UserCmdDemo::parseFromFile(const char *filename) {
    const std::pair<std::vector<char>, bool> result = readWholeFile(filename);
    if (!result.second) {
        ConMsg( "%s does not exist.\n", filename );
        return nullptr; // TODO: error message?
    }
    const std::vector<char>& data = result.first;
    demoheader_t header;
    memcpy(&header, &data[0], sizeof(demoheader_t));
    ByteSwap_demoheader_t(header);

    if (Q_strcmp(header.demofilestamp, DEMO_HEADER_ID )) {
        ConMsg("%s has invalid demo header ID.\n", filename);
        return nullptr;
    }
    if (header.networkprotocol != PROTOCOL_VERSION) {
        ConMsg("ERROR: %s has different protocol version (%d), expected %d\n", filename, header.networkprotocol, PROTOCOL_VERSION);
        return nullptr;
    }
    if ( ( header.demoprotocol > DEMO_PROTOCOL) || ( header.demoprotocol < 2 ) )
    {
        ConMsg ("ERROR: demo file protocol %i outdated, engine not version is %i \n",
                header.demoprotocol, DEMO_PROTOCOL );

        return NULL;
    }

    std::vector<CUserCmd> cmds = parseCmds(&data[sizeof(demoheader_t)], data.size() - sizeof(demoheader_t));
    return new UserCmdDemo(header, std::move(cmds));
}


#endif //USERCMDDEMO_H
